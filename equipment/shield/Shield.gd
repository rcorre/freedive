extends Spatial

onready var anim_player: AnimationPlayer = $AnimationPlayer

func set_active(active: bool):
	if active:
		anim_player.play("On")
	else:
		anim_player.play("Off")
