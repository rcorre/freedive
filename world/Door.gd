extends MeshInstance

onready var anim_player: AnimationPlayer = $AnimationPlayer

func _ready():
	$Area.connect("body_entered", self, "_on_body_entered")
	$Area.connect("body_exited", self, "_on_body_exited")

func _on_body_entered(_body: Node):
	anim_player.play("Open")

func _on_body_exited(_body: Node):
	anim_player.play_backwards("Open")
