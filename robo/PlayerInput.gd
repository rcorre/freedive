extends Node
class_name PlayerInput

const MOUSE_SENSITIVITY = Vector2(0.005, 0.005)

signal look(direction)
signal move(direction)
signal dash(direction)
signal fire_left(is_firing)
signal fire_right(is_firing)

var mouse_delta: Vector2

func _input(ev: InputEvent):
	var mouse_motion := ev as InputEventMouseMotion
	if mouse_motion:
		mouse_delta.y += mouse_motion.relative.y * MOUSE_SENSITIVITY.y
		mouse_delta.x += mouse_motion.relative.x * MOUSE_SENSITIVITY.x

func _physics_process(delta: float):
	if mouse_delta.length() > 0:
		emit_signal("look", mouse_delta * delta)
		mouse_delta = Vector2()

	var move_target := Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_forward") - Input.get_action_strength("move_backward")
	).normalized()

	if Input.is_action_just_pressed("dash"):
		emit_signal("dash", move_target)
	else:
		emit_signal("move", move_target)

	if Input.is_action_just_pressed("attack_right"):
		emit_signal("fire_right", true)
	elif Input.is_action_just_released("attack_right"):
		emit_signal("fire_right", false)

	if Input.is_action_just_pressed("attack_left"):
		emit_signal("fire_left", true)
	elif Input.is_action_just_released("attack_left"):
		emit_signal("fire_left", false)
