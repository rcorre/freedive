extends Node
class_name AIInput

signal look(direction)
signal move(direction)
signal dash(direction)
signal fire_left(is_firing)
signal fire_right(is_firing)

func _ready():
	var timer = Timer.new()
	add_child(timer)
	timer.connect("timeout", self, "_on_timer_timeout")
	timer.start(1.0)

func _on_timer_timeout():
	match randi() % 5:
		0: 
			emit_signal("move", Vector2(1, 0))
		1: 
			emit_signal("move", Vector2(-1, 0))
		2: 
			emit_signal("fire_right", true)
		3: 
			emit_signal("dash", Vector2(-1, 0))
		4: 
			emit_signal("dash", Vector2(1, 0))
