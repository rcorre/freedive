extends KinematicBody

const HEAT_DISSIPATION := 20.0
const ROTATION_INTERPOLATE_SPEED := 6
const ACCEL := 3

signal dash()
signal heat_changed(value)
signal overheat(is_overheating)

onready var anim_tree: AnimationTree = $AnimationTree
onready var camera_pivot: Spatial = $CameraPivot
onready var camera_tilt: Spatial = $CameraPivot/CameraTilt
onready var body: Spatial = $Armature
onready var fire_point: Spatial = $FirePoint
onready var shield: Spatial = $FirePoint/Shield

onready var bullet: PackedScene = preload("res://robo/Bullet.tscn")

export(NodePath) var target: NodePath
var fire_time := 0.0
var move_target: Vector2
var velocity: Vector2

var firing := false
var defending := false
var overheating := false
var heat := 0.0

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	var input := $Input
	input.connect("look", self, "_on_look")
	input.connect("move", self, "_on_move")
	input.connect("dash", self, "_on_dash")
	input.connect("fire_right", self, "_on_fire")
	input.connect("fire_left", self, "_on_defend")

func _on_defend(is_defending: bool):
	defending = is_defending
	shield.set_active(is_defending)

func _on_look(direction: Vector2):
	camera_tilt.rotate(Vector3.LEFT, direction.x)
	camera_pivot.rotate(Vector3.UP, direction.y)

func _on_move(direction: Vector2):
	move_target = direction

func _on_dash(direction: Vector2):
	if not overheating:
		emit_signal("dash")
		anim_tree["parameters/dash_direction/blend_position"] = direction
		anim_tree["parameters/dash/active"] = true
		velocity = direction
		firing = false
		heat += 30
		if heat >= 100.0:
			emit_signal("overheat", true)
			overheating = true

func _on_fire(is_firing: bool):
	firing = is_firing and not anim_tree["parameters/dash/active"]

func _physics_process(delta: float):
	fire_time = max(fire_time - delta, 0)

	if firing and fire_time <= 0 and not overheating:
		heat += 10.0
		emit_signal("heat_changed", heat)
		fire_time = 0.1
		var b = bullet.instance()
		get_tree().current_scene.add_child(b)
		b.global_transform = fire_point.global_transform
		if heat >= 100.0:
			emit_signal("overheat", true)
			overheating = true
	elif heat > 0:
		heat = max(0, heat - delta * HEAT_DISSIPATION)
		emit_signal("heat_changed", heat)
		if overheating and heat == 0:
			overheating = false
			emit_signal("overheat", false)

	#if move_target.length() > 0.01:
	#	var q_from = body.global_transform.basis.get_rotation_quat()
	#	var q_to = (
	#		camera_pivot.global_transform.basis.rotated(Vector3.UP, move_target.angle())
	#	).get_rotation_quat()
	#	body.global_transform.basis = Basis(q_from.slerp(q_to, delta * ROTATION_INTERPOLATE_SPEED))

	if firing or defending:
		move_target = move_target / 3
	
	velocity = velocity.move_toward(move_target, ACCEL * delta)
	anim_tree["parameters/move/blend_position"] = velocity

	#move_target = global_transform.basis.xform(move_target)
	var root_motion := anim_tree.get_root_motion_transform()
	#body.global_transform.basis *= root_motion.basis

	#global_transform.basis = root_motion.basis
	move_and_slide(body.global_transform.basis.xform(root_motion.origin) / delta, Vector3(0, 1, 0))

	if target:
		var target_xz := (get_node(target) as Spatial).global_transform.origin
		target_xz.y = 0
		look_at(target_xz, Vector3.UP)
		rotate_y(PI)

		#var target_yz := target.global_transform.origin
		#target_xz.x = 0
		#camera_tilt.look_at(target_yz, Vector3.LEFT)
