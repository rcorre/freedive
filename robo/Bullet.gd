extends KinematicBody

const speed := 30

func _physics_process(delta: float):
	var velocity = global_transform.basis.z * speed
	var hit = move_and_collide(velocity * delta)
	if hit:
		queue_free()
